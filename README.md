# Documentation of the User-Service:

## Main purpose of the User-Service?

The User-Service is used to save additional user data in the user database as well as to edit existing entries.
When a new User signs up it is created at the [Auth Service](https://git.fim.uni-passau.de/ep/ws20_21/team_dis_1/auth) as well as at the User Service. Therefore it does not save information (email, password) that is needed to login.

## Internal Service Logic:
The User Service consists of two components, the MariaDB database and the Java Spring Application.\
The service depends on the Discovery Service.
It has to be available in order to start the service and assure correct behaviour.

The Spring Application consists of these parts:
- User Class
    - contains the basic user information (id, username, query limits, query counter, darkmode setting)
- UserService Class
    - contains the interactions between the Controller and the table entries.\
      It receives information like id, username, ... and changes the database entries or returns found entries or exceptions when an error occures.\
      More information is available in the links below.
- QueryRepository Class
    - manages the CRUD operations.
- RESTController
    - contains the mapping for the commmunication with the frontend and the matching service method calls.
- Exceptions (IllegalCounterException, IllegalParameterException, NoSuchUserException)
    - handles all possible errrors and returns a HttpStatus code with the error message.

The MariaDB database consists of these parts:
A table (user) to save instances of registered users.
It saves the following data for a user when a new ID is received from the Auth-Service:
- id
- username
- query limit
- query counter
- darkmode setting

It is then possible to edit these values via mappings in the RESTController.

More information about the values is in the User-Service [Database Wiki](https://git.fim.uni-passau.de/ep/ws20_21/team_dis_1/user/-/wikis/User-Database).

## Currently Available Endpoints:

There are mappings for most of the database / table entries in order to receive and update existing entries as well as to create new ones.

A full list of available endpoints is in the User-Service [Wiki](https://git.fim.uni-passau.de/ep/ws20_21/team_dis_1/user/-/wikis/User-API).

## Error Responses

In the case of errors, the API responds with 4** or 5** HTTP codes. The response body contains information about the time, type, message and details of the error. The detail field is optional.