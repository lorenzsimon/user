FROM maven:3.6.3-jdk-11
COPY ./service .
RUN mvn package -Dmaven.test.skip=true

FROM openjdk:11
COPY --from=0 /target/*.jar .
CMD java -jar *.jar

#FROM mariadb:latest
#ADD ./service/src/main/resources/sql/tables.sql /docker-entrypoint-initdb.d
EXPOSE 3306