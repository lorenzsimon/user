package de.unipassau.ep.boogle.controller;

import de.unipassau.ep.boogle.application.UserService;
import de.unipassau.ep.boogle.dto.SignupDTO;
import de.unipassau.ep.boogle.dto.UserDto;
import de.unipassau.ep.boogle.persistence.entities.User;
import de.unipassau.ep.logging.service.LoggingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * A {@link Controller} that manages the {@link PostMapping}s,
 * {@link DeleteMapping}s and the {@link GetMapping}s of the service.
 * It contains methods to save, delete and update user entries in the
 * MariaDB database.
 */
@RestController
public class RESTController {
    private final UserService userService;
    private final LoggingService loggingService;

    /**
     * Constructor that sets the {@link UserService} and {@link LoggingService}.
     * @param userService The {@link UserService}.
     * @param loggingService The {@link LoggingService}.
     */
    @Autowired
    public RESTController(UserService userService,LoggingService loggingService) {
        this.userService = userService;
        this.loggingService = loggingService;
    }

    /**
     * Returns the basic information for a user in the database.
     * @param userid The ID to check.
     * @return a {@link UserDto} with the basic information in JSON format,
     * or an empty object if there is no such user.
     */
    @GetMapping("/user/{userid}")
    public ResponseEntity<String> getUserInformation(@PathVariable int userid) {
        loggingService.add("apiEndpoint","/user/{userid}").send("api");
        return new ResponseEntity<>(
                new UserDto(userService.getUser(userid)).toString(),
                HttpStatus.OK);
    }

    /**
     * Checks the Darkmode setting for the referenced {@link User} and returns it.
     * @param userid The ID to check.
     * @return {@code true} if and only if the user was found and has the
     * darkmode setting on {@code true}, otherwise {@code false}.
     */
    @GetMapping("/user/{userid}/darkmode")
    public ResponseEntity<Boolean> getUserDarkModeSetting(@PathVariable int userid) {
        loggingService.add("apiEndpoint","/user/{userid}/darkmode").send("api");
        return new ResponseEntity<>(
                userService.getDarkModeSetting(userid),
                HttpStatus.OK);
    }

    /**
     * Updates the user's query counter.
     * @param userid The ID to check.
     * @return The {@code userid} if and only if the user was found and the counter updated.
     */
    @GetMapping("/user/{userid}/updatecounter")
    public ResponseEntity<String> incrementQueryCounter(@PathVariable int userid) {
        loggingService.add("apiEndpoint","/user/{userid}/updatecounter").send("api");
        return new ResponseEntity<>(
                userService.incCounter(userid),
                HttpStatus.OK);
    }

    /**
     * Resets the user's query counter.
     * @param userid The ID to check.
     * @return The {@code userid} if and only if the user was found and the counter rest.
     */
    @GetMapping("/user/{userid}/resetcounter")
    public ResponseEntity<String> resetQueryCounter(@PathVariable int userid) {
        loggingService.add("apiEndpoint","/user/{userid}/resetcounter").send("api");
        return new ResponseEntity<>(
                userService.resetCounter(userid),
                HttpStatus.OK);
    }


    /**
     * Returns the users query limit.
     * @param userid The ID to check.
     * @return The current query limit.
     */
    @GetMapping("/user/{userid}/limit")
    public ResponseEntity<Integer> getQueryLimit(@PathVariable int userid) {
        loggingService.add("apiEndpoint","/user/{userid}/limit").send("api");
        return new ResponseEntity<>(
                userService.getQueryLimit(userid),
                HttpStatus.OK);
    }

    /**
     * Sets the query limit of a user.
     * @param userid The ID to check.
     * @param limit The new query limit.
     * @return The user id if the user limit was set.
     */
    @PostMapping("/user/{userid}/editlimit")
    public ResponseEntity<String> editQueryLimit(@PathVariable int userid, @RequestBody String limit) {
        loggingService.add("apiEndpoint","/user/{userid}/editlimit").send("api");
        //return userService.editQueryLimit(userid, Integer.parseInt(limit.substring(0, limit.length()-1)));
        return new ResponseEntity<>(
                userService.editQueryLimit(userid, limit),
                HttpStatus.OK);
    }

    /**
     * Sets the darkmode setting for a {@link User} to the given value.
     * The RequestParam must be called darkmode.
     * @param userid The ID to check.
     * @param darkmode The new darkmode setting.
     * @return A {@link String} containing the {@code userid} if it was
     * correctly changed.
     */
    @PostMapping ("/user/{userid}/editdarkmode")
    public ResponseEntity<String> changeDarkModeSetting(@PathVariable int userid, @RequestBody String darkmode) {
        loggingService.add("apiEndpoint","/user/{userid}/editdarkmode").send("api");
        return new ResponseEntity<>(
                userService.changeDarkModeSetting(userid, darkmode),
                HttpStatus.OK);
    }

    /**
     * Requests a username for a given UD.
     * @param userid The ID to check.
     * @return A {@link String} that contains the username.
     */
    @GetMapping("/user/{userid}/username")
    public ResponseEntity<String> getUsername(@PathVariable int userid) {
        loggingService.add("apiEndpoint","/user/{userid}/username").send("api");
        return new ResponseEntity<>(
                userService.getUsername(userid),
                HttpStatus.OK);
    }

    /**
     * Sets the username for the given ID.
     * @param userid The ID to check.
     * @param username The new username.
     * @return The {@code userid} if and only if ID exists and the username was
     * changed.
     */
    @PostMapping("/user/{userid}/editusername")
    public ResponseEntity<String> changeUsername(@PathVariable int userid, @RequestBody String username) {
        loggingService.add("apiEndpoint","/user/{userid}/editusername").send("api");
        System.out.println("userid: " + userid);
        return new ResponseEntity<>(
                userService.setUsername(userid, username),
                HttpStatus.OK);
    }

    /**
     * Returns a {@link List} of all users currently saved in the database.
     * @return A {@link List} of {@link User}s saved in the database.
     */
    @GetMapping("/useroverview")
    public ResponseEntity<List<User>> getAllUsers() {
        loggingService.add("apiEndpoint","/useroverview").send("api");
        return new ResponseEntity<>(
                userService.getAllUsers(),
                HttpStatus.OK);
    }


    /**
     * Registers a new user.
     * @param signupDTO The {@link SignupDTO} containing username and id.
     * @return A {@link String} containing the user id.
     */
    @PostMapping("/signup")
    public ResponseEntity<String> signup(@RequestBody SignupDTO signupDTO) {
        return new ResponseEntity<>(
                userService.signup(signupDTO),
                HttpStatus.OK);
    }

    /**
     * Detelets a user from the database.
     * @param userid The user id.
     * @return A {@link String} containing the deleted user id.
     */
    @DeleteMapping("/user/{userid}")
    public ResponseEntity<String> delete(@PathVariable int userid) {
        return new ResponseEntity<>(
                userService.deleteUser(userid),
                HttpStatus.OK);
    }
}
