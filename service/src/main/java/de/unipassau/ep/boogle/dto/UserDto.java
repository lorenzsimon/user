package de.unipassau.ep.boogle.dto;

import de.unipassau.ep.boogle.application.exception.IllegalParameterException;
import de.unipassau.ep.boogle.application.exception.NoSuchUserException;
import de.unipassau.ep.boogle.persistence.entities.User;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Used to return the information of a single user without its id.
 */
public class UserDto {
    private boolean darkMode;
    private int queryCounter;
    private int queryLimit;
    private String username;

    /**
     * Constructor that sets {@link User} information.
     * @param user The given {@link User}.
     * @throws NoSuchUserException No user was found.
     */
    public UserDto(User user) throws IllegalParameterException {
        if (user == null) {
            throw new IllegalParameterException("User not found");
        }

        darkMode = user.getDarkMode();
        queryCounter = user.getQueryCounter();
        queryLimit = user.getQueryLimit();
        username = user.getUsername();
    }

    /**
     * Returns the user's information.
     * @return {@link String} containing the user's data in JSON format.
     */
    @Override
    public String toString() {
        String text = "{\n"
                + "\"username\":\"" + username + "\",\n"
                + "\"darkode\":\"" + darkMode + "\",\n"
                + "\"querycounter\":\"" + queryCounter + "\",\n"
                + "\"querylimit\":\"" + queryLimit + "\"\n"
                + "}";
        return text;
    }
}
