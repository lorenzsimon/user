package de.unipassau.ep.boogle.application;

import de.unipassau.ep.boogle.application.exception.IllegalCounterException;
import de.unipassau.ep.boogle.application.exception.IllegalParameterException;
import de.unipassau.ep.boogle.application.exception.NoSuchUserException;
import de.unipassau.ep.boogle.dto.SignupDTO;
import de.unipassau.ep.boogle.persistence.entities.User;
import de.unipassau.ep.boogle.persistence.repositories.UserRepository;
import de.unipassau.ep.logging.service.LoggingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.LinkedList;
import java.util.List;

/**
 * Enables the interaction with the {@link User} table.
 * The interaction contains the creating, deleting and changing
 * entries.
 */
@Service
public class UserService {
    private final UserRepository userRepository;
    private final String USER_NOT_FOUND = "user not found";
    private final String INVALID_PARAMETER = "invalid parameter";
    private final String COUNTER_LIMIT_REACHED = "counter limit reached";
    private final String JSON_KEY_USERNAME = "username";
    private final String JSON_KEY_DARKMODE = "darkmode";
    private final String JSON_KEY_QUERYLIMIT = "queryLimit";
    private final LoggingService loggingService;

    /**
     * Constructor that initializes the {@link UserRepository} and {@link LoggingService}.
     * @param userRepository The {@link UserRepository}.
     * @param loggingService The {@link LoggingService}.
     */
    @Autowired
    public UserService(UserRepository userRepository, LoggingService loggingService) {
        this.userRepository = userRepository;
        this.loggingService = loggingService;
    }

    /**
     * Returns a {@link User} object that contains the user information for
     * the given ID.
     * @param id The ID to check.
     * @return A {@link User} object that contains the user information.
     * @throws NoSuchUserException No user was found.
     */
    public User getUser(int id) throws NoSuchUserException {
        loggingService.add("userID",id).add("action","getUser").send("userdata");
        if (!idExists(id)) {
            throw new NoSuchUserException(USER_NOT_FOUND);
        }

        return userRepository.findById(id);
    }


    /**
     * Deletes a user from the table for the given ID.
     * @param id The ID to check.
     * @return A {@link String} containing the user id.
     * @throws NoSuchUserException No user was found.
     */
    public String deleteUser(int id) {
        LoggingService.LoggingBuilder logging = loggingService.add("userID",id).add("action","deleteUser");
        if (!idExists(id)) {
            throw new NoSuchUserException(USER_NOT_FOUND);
        }
        User user = userRepository.findById(id);
        userRepository.delete(user);
        logging.add("success","true").send("userdata");
        return "" + user.getId();
    }

    /**
     * Updates the query counter for a user referenced by the ID.
     * @param id The ID to check.
     * @return The {@code id} if and only if the id was found in the table and
     * the counter was updated.
     * @throws NoSuchUserException No user was found.
     * @throws IllegalCounterException The counter limit was reached.
     */
    public String incCounter(int id) throws NoSuchUserException, IllegalCounterException {
        LoggingService.LoggingBuilder logging = loggingService.add("userID",id).add("action","incCounter");

        if (!idExists(id)) {
            throw new NoSuchUserException(USER_NOT_FOUND);
        }

        User user = userRepository.findById(id);

        if (user.getQueryCounter() >= user.getQueryLimit()) {
            throw new IllegalCounterException(COUNTER_LIMIT_REACHED);
        }

        logging.add("old",user.getQueryCounter());
        user.incQueryCounter();
        userRepository.save(user);
        logging.add("new",user.getQueryCounter()).add("success","true").send("userdata");
        return "" + id;
    }

    /**
     * Resets the query counter for a user specified via the ID.
     * @param id The ID to check.
     * @return {@code id} if and only if the user was found and the
     * counter was reset.
     * @throws NoSuchUserException No user was found.
     */
    public String resetCounter(int id) throws NoSuchUserException {
        LoggingService.LoggingBuilder logging = loggingService.add("userID",id).add("action","resetCounter");

        if (!idExists(id)) {
            throw new NoSuchUserException(USER_NOT_FOUND);
        }

        User user = userRepository.findById(id);
        logging.add("old",user.getQueryCounter());
        user.resetQueryCounter();
        userRepository.save(user);
        logging.add("new",user.getQueryCounter()).add("success","true").send("userdata");
        return "" + id;
    }

    /**
     * Changes the darkmode setting of the given user.
     * @param id The ID to check.
     * @param darkmode The new value of the darkmode.
     * @return The {@code id} if the user was found and the
     * darkmode setting was changed.
     * @throws NoSuchUserException No user was found.
     */
    public String changeDarkModeSetting(int id, String darkmode) throws NoSuchUserException {
        LoggingService.LoggingBuilder logging = loggingService.add("userID",id).add("action","changeDarkModeSetting");

        if (!idExists(id)) {
            throw new NoSuchUserException(USER_NOT_FOUND);
        }

        User user = userRepository.findById(id);
        logging.add("old",user.getDarkMode());
        // parseJSON returns the String value and this is converted to a boolean
        user.setDarkMode(Boolean.parseBoolean(parseJSON(darkmode, JSON_KEY_DARKMODE)));
        userRepository.save(user);
        logging.add("new",user.getDarkMode()).add("success","true").send("userdata");
        return "" + id;
    }

    /**
     * Returns the darkmode setting for the given user.
     * @param id The id to check.
     * @return The current value of the darkmode.
     * @throws NoSuchUserException No user was found.
     */
    public boolean getDarkModeSetting(int id) throws NoSuchUserException {
        LoggingService.LoggingBuilder logging = loggingService.add("userID",id).add("action","getDarkModeSetting");

        if (!idExists(id)) {
            throw new NoSuchUserException(USER_NOT_FOUND);
        }

        User user = userRepository.findById(id);
        boolean mode = user.getDarkMode();
        logging.add("value",mode).add("success",true).send("userdata");
        return mode;
    }

    /**
     * Adds a new user (id and username).
     * @param signupDTO The {@link SignupDTO} containing username and id.
     * @return A {@link String} containing the user id.
     * @throws IllegalParameterException The DTO object is {@code null} or
     * the user id is already in use.
     */
    public String signup(SignupDTO signupDTO) throws IllegalParameterException {
        if (signupDTO == null || idExists(signupDTO.getId())) {
            throw new IllegalParameterException(INVALID_PARAMETER);
        }

        User user = new User(signupDTO);
        userRepository.save(user);
        return "" + user.getId();
    }

    /**
     * Saves a new {@link User} in the table if the id does not already exist and
     * returns a message.
     * @param id The ID to add.
     * @return A {@link String} the user id.
     * @throws IllegalParameterException The user id is already in use.
     */
    public String saveUserById(int id) throws IllegalParameterException {
        LoggingService.LoggingBuilder logging = loggingService.add("userID",id).add("action","saveUserById");
        if (idExists(id)) {
            logging.add("success",false).send("userdata");
            throw new IllegalParameterException(INVALID_PARAMETER);
        }
        User user = new User(id);
        userRepository.save(user);
        logging.add("success",true).send("userdata");
        return "" + user.getId();
    }

    private boolean idExists(int id) {
        boolean value = userRepository.existsById(id);// userRepository.findById(id) != null;
        loggingService.add("userID",id).add("action","idExists").add("success",value).send("userdata");
        return value;
    }

    /**
     * Checks if the user ID exists and then return the {@link User}s username
     * if it does.
     * @param id The ID to check
     * @return A {@link String} containing the username.
     * @throws NoSuchUserException No user was found.
     */
    public String getUsername(int id) throws NoSuchUserException {
        LoggingService.LoggingBuilder logging = loggingService.add("userID",id).add("action","getUsername");

        if (!idExists(id)) {
            throw new NoSuchUserException(USER_NOT_FOUND);
        }

        String name = userRepository.findById(id).getUsername();
        logging.add("value",name).add("success",true).send("userdata");
        return name;
    }

    /**
     * Sets the username if there is a {@link User} for the given ID.
     * @param id The ID to check.
     * @param username A {@link String} containing the username.
     * @return The {@code id} if and only if the username was changed.
     * @throws NoSuchUserException No user was found.
     * @throws IllegalParameterException The parameter was incorrect
     * (> 10 characters).
     */
    public String setUsername(int id, String username) throws NoSuchUserException, IllegalParameterException {
        LoggingService.LoggingBuilder logging = loggingService.add("userID",id).add("action","setUsername");

        username = parseJSON(username, JSON_KEY_USERNAME);

        if (!idExists(id)) {
            throw new NoSuchUserException(USER_NOT_FOUND);
        } else if (username.length() > 10) {
            throw new IllegalParameterException(INVALID_PARAMETER);
        }

        User user = userRepository.findById(id);
        logging.add("old",user.getUsername());
        user.setUsername(username);
        userRepository.save(user);
        logging.add("new",user.getUsername()).add("success",true).send("userdata");
        return "" + id;
    }

    /**
     * Returns a list of all currently registered users with the saved
     * information.
     * @return A {@link List} of {@link User}s or an empty {@link List} if
     * there are no users.
     */
    public List<User> getAllUsers() {
        LoggingService.LoggingBuilder logging = loggingService.add("action","getAllUsers");
        List<User> users = new LinkedList<>();
        for (User user: userRepository.findAll()) {
            users.add(user);
        }
        logging.add("value",users.size()).send("usage");
        /* returns a List of User objects since the UserDTO doesn't
         * store the ID and the Admin should also see the that. */
        return users;
    }

    /**
     * Returns the query limit of the given user.
     * @param id The user ID.
     * @return The {@code queryLimit} of the user.
     * @throws NoSuchUserException No user was found.
     */
    public int getQueryLimit(int id) throws NoSuchUserException {
        if (!idExists(id)) {
            throw new NoSuchUserException(USER_NOT_FOUND);
        }
        return userRepository.findById(id).getQueryLimit();
    }

    /**
     * Sets the new query limit for a user.
     * @param id The user ID to check.
     * @param limit The new query limit.
     * @return The {@code id} if the new limit was set, otherwise
     * {@link NoSuchUserException} if no such user was found, or
     * {@link IllegalParameterException} if the query limit
     * is incorrect.
     * @throws IllegalParameterException The new query limit is < 0.
     * @throws NoSuchUserException No user was found.
     */
    public String editQueryLimit(int id, String limit) throws IllegalParameterException, NoSuchUserException {
        int queryLimit = Integer.parseInt(parseJSON(limit, JSON_KEY_QUERYLIMIT));

        if(queryLimit < 0) {
            throw new IllegalParameterException(INVALID_PARAMETER);
        } else if (!idExists(id)) {
            throw new NoSuchUserException(USER_NOT_FOUND);
        }

        User user = userRepository.findById(id);
        user.setQueryLimit(queryLimit);
        userRepository.save(user);
        return "" + user.getId();
    }

    @PostConstruct
    private void addTestDataSet() {
        // adds the admin to the user database
        if (!idExists(0)) {
            User admin = new User(0);
            admin.setUsername("admin");
            admin.setQueryLimit(1000);
            admin.setQueryCounter(0);
            admin.setDarkMode(true);
            userRepository.save(admin);
        }
    }

    private static String parseJSON(String json, String key) {
        // Parses the value of a String in the JSON format.
        // Returns the value matching the given key or null
        org.json.JSONObject jsonObject = new org.json.JSONObject(json);
        Object object = jsonObject.get(key);
        System.out.println("key: " + key);
        if (object instanceof Boolean) {
            return "" + jsonObject.getBoolean(key);
        } else if (object instanceof String) {
            return jsonObject.getString(key);
        } else if (object instanceof Integer) {
            return "" + jsonObject.getInt(key);
        }
        return null;
    }
}
