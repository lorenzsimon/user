package de.unipassau.ep.boogle.persistence.repositories;

import de.unipassau.ep.boogle.persistence.entities.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * A {@link Repository} that is used to identify a {@link User} table entry
 * by the email address.
 */
@Repository
public interface UserRepository extends CrudRepository<User, String> {
    /**
     * Iterates through the table in order to find a {@link User} object
     * that contains the given ID.
     * @param id The ID to check.
     * @return The {@link User} object if a user was found, otherwise
     * {@code null}.
     */
    User findById(int id);

    /**
     * Iterates through the table in order to find an entry with the given id.
     * @param id The id to check.
     * @return {@code true} if and only if a user was found that matches the
     * id, otherwise {@code false}.
     */
    boolean existsById(int id);
}
