package de.unipassau.ep.boogle.dto;

import lombok.Data;

/**
 * This class contains the id and username for new users.
 * It is used to receive data from the frontend.
 */
@Data
public class SignupDTO {
    private int id;
    private String username;

    /**
     * Constructor of the class that sets the arguments.
     * @param id The user id.
     * @param username The {@link String} containing the username.
     */
    public SignupDTO(int id, String username) {
        this.id = id;
        this.username = username;
    }
}
