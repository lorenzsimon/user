package de.unipassau.ep.boogle;

import de.unipassau.ep.logging.service.LoggingService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

/**
 * The class provides the main method and is used to configure the application.
 */
@SpringBootApplication
public class BoogleApplication {

	/**
	 * Represent the start point of the application.
	 * @param args Arguments that are passed at the start.
	 */
	public static void main(String[] args) {
		SpringApplication.run(BoogleApplication.class, args);
	}

	/**
	 * Declares the {@code LoggingService} bean.
	 * @return The {@code LoggingService} managed by spring.
	 */
	@Bean
	@Scope("singleton")
	public LoggingService loggingService() {
		return new LoggingService("user");
	}

}
