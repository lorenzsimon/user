package de.unipassau.ep.boogle.application.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A IllegalParameterException shows that the parameters of a method
 * does not match the required fields.
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class IllegalParameterException extends IllegalArgumentException {
    /**
     * Creates IllegalParameterException with a message.
     * @param msg a {@link String} containing the error message.
     */
    public IllegalParameterException(final String msg) {
        super(msg);
    }
}
