package de.unipassau.ep.boogle.application.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A NoSuchUserException indicates, that a user was specified that does not
 * exist in the user database.
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public final class NoSuchUserException extends RuntimeException {

    /**
     * Creates a NoSuchUserException with the given message.
     * @param msg a {@link String} containing the error message.
     */
    public NoSuchUserException(final String msg) {
        super(msg);
    }
}
