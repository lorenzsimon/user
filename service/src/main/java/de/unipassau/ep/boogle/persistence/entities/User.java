package de.unipassau.ep.boogle.persistence.entities;

import de.unipassau.ep.boogle.dto.SignupDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;


/**
 * The class {@code User} describes the table in the database dbuser.
 * It an entity of the table containing the user data.
 * It saves an id for each user as well as the dark mode settings,
 * query limit and query counter.
 */
@Entity
public class User implements Serializable {
    private final int USERNAME_MAX_LENGTH = 10;
    private final int MAX_QUERIES_DEFAULT = 100;

    @Id
    private int id;
    private boolean darkMode;
    private int queryLimit;
    private int queryCounter;

    @NotNull
    @Size(max = USERNAME_MAX_LENGTH)
    private String username;

    /**
     * The default constructor. There's currently no cases that uses it.
     */
    public User() {
    }

    /**
     * Constructor that creates a new database / table entry for the given ID.
     * @param id The ID to save.
     */
    public User(int id) {
        this.id = id;
        darkMode = false;
        queryCounter = 0;
        queryLimit = MAX_QUERIES_DEFAULT;
        username="";
    }

    /**
     * Constructor for a {@link SignupDTO} that contains username and id.
     * @param signupDTO A {@link SignupDTO}.
     */
    public User(SignupDTO signupDTO) {
        this.id = signupDTO.getId();
        this.username = signupDTO.getUsername();
        darkMode = false;
        queryCounter = 0;
        queryLimit = MAX_QUERIES_DEFAULT;
    }

    /**
     * Returns the user ID.
     * @return The user ID.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the user ID.
     * @param id The new user ID.
     */
    public void setId(int id) {
        if (id > 0) {
            this.id = id;
        }
    }

    /**
     * Returns the user's dark mode setting.
     * @return A {@code boolean} describing the user's dark mode setting.
     */
    public boolean getDarkMode() {
        return darkMode;
    }

    /**
     * Sets the user's dark mode setting to the given value.
     * @param darkMode The new dark mode setting value.
     */
    public void setDarkMode(boolean darkMode) {
        this.darkMode = darkMode;
    }

    /**
     * Returns the user's query limit.
     * @return An {@link int} containing the user's query limit.
     */
    public int getQueryLimit() {
        return queryLimit;
    }

    /**
     * Sets the user's query limit to the given value.
     * @param queryLimit The new query limit.
     */
    public void setQueryLimit(int queryLimit) {
        if (queryLimit > 0) {
            this.queryLimit = queryLimit;
        }
    }


    /**
     * Returns the user's query counter.
     * @return An {@link int} containing the user's query counter.
     */
    public int getQueryCounter() {
        return queryCounter;
    }

    /**
     * Sets the user's query counter to the given value.
     * @param queryCounter The new query counter.
     */
    public void setQueryCounter(int queryCounter) {
        if (queryCounter > 0) {
            this.queryCounter = queryCounter;
        }
    }

    /**
     * Increments the user's query counter.
     */
    public void incQueryCounter() {
        queryCounter++;
    }

    /**
     * Resets the user's query counter to 0.
     */
    public void resetQueryCounter() {
        queryCounter = 0;
    }

    /**
     * Sets the user's new username or keeps it at the old one, if {@code null}
     * was received or the username was longer than expected.
     * The default username is an empty {@link String}.
     * @param username A {@link String} containing the new username.
     * @return Returns the username if it was set correctly, otherwise
     * "invalid username".
     */
    public String setUsername(String username) {
        if (username == null || username.length() > USERNAME_MAX_LENGTH) {
            return "invalid username";
        }
        this.username = username;
        return username;
    }

    /**
     * Returns the user's username.
     * @return {@link String} containing the user's username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Returns the user's information.
     * @return {@link String} containing the user's data in JSON format.
     */
    @Override
    public String toString() {
        return "User{\n"
                + "\"id\"=\"" + id + "\",\n"
                + "\"username\"=\"" + username + "\",\n"
                + "\"darkMode\"=\"" + darkMode + "\",\n"
                + "\"queryLimit\"=\"" + queryLimit + "\",\n"
                + "\"queryCounter\"=\"" + queryCounter + "\"\n"
                + "}";
    }
}
